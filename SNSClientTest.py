# Test of subscribing to SNS via a SQS Queue
import boto3, uuid, json, atexit

# Generates a policy with the SNS topic and SQS Queue ARNs included. Required for subscribing queue to topic.
# https://stackoverflow.com/questions/48738509/aws-empty-sqs-queue-when-subscribed-to-sns-via-boto3/48748295#48748295
def allow_sns_to_write_to_sqs(topicArn, queueArn):
    policy_document = """{{
  "Version":"2012-10-17",
  "Statement":[
    {{
      "Sid":"MyPolicy",
      "Effect":"Allow",
      "Principal" : {{"AWS" : "*"}},
      "Action":"SQS:SendMessage",
      "Resource": "{}",
      "Condition":{{
        "ArnEquals":{{
          "aws:SourceArn": "{}"
        }}
      }}
    }}
  ]
}}""".format(queueArn, topicArn)
    return policy_document

# Deletes queue when program is terminated
def exitHandler():
    print('Deleting Queue')
    sqsClient.delete_queue(QueueUrl = queue.url)
    snsClient.unsubscribe(SubscriptionArn = subscription['SubscriptionArn'])
    print('Queue Deleted and Unsubscribed')
atexit.register(exitHandler)


# Create a SQS Queue. QueueName will be a UUID.
# Long Polling to ensure every server is queried, and fewer empty responses are returned
sqsClient = boto3.client('sqs')
queue = boto3.resource('sqs').create_queue(QueueName=str(uuid.uuid4()), Attributes={'ReceiveMessageWaitTimeSeconds': '20'})
queueArn = queue.attributes['QueueArn']
print('Created SQS queue with ARN of ' + queue.attributes['QueueArn'])

#The ARN of the topic we'll be subscribing to
topicArn = 'arn:aws:sns:ap-southeast-2:519320755341:Tennant1-Products'


# Subscribe queue to SNS
snsClient = boto3.client('sns')
subscription = snsClient.subscribe(TopicArn=topicArn, Protocol='sqs', Endpoint=queueArn)
print('Queue subscribed')

# Apply the policy to the queue.
policy_json = allow_sns_to_write_to_sqs(topicArn, queueArn)
response = sqsClient.set_queue_attributes(
    QueueUrl = queue.url,
    Attributes = {
        'Policy' : policy_json
    }
)

# Print every incoming message
while (True):
    for message in queue.receive_messages():
        #Message from SNS is in JSON format. Convert to dictionary and print the messsage field.
        print(json.loads(message.body)['Message'])
        message.delete()
