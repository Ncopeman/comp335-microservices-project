import boto3
import sqlite3 as sqlite
import json
import uuid
import datetime
import pickle
import binascii
import atexit

tennant_name = "Tennant1"
microservice_name = str(uuid.uuid4()) #Required for Response or Write Queues
WC = binascii.hexlify(pickle.dumps("%")).decode("utf-8")

#Connect to DB and Define GUID Conversions
sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

#Build Sessions and Clients
session = boto3.Session(aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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', region_name='ap-southeast-2')
sqs = session.resource("sqs", region_name = "ap-southeast-2")
read_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_cookies")


#Set Up Write Queue
sqs_client = boto3.client("sqs", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
sns_client = boto3.client("sns", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
write_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})
queue_arn = write_queue.attributes['QueueArn']
change_queue = sns_client.list_topics()["Topics"]
for value in change_queue:
    value = value["TopicArn"]
    if tennant_name + "_cookies" in value:
        topic_arn = value
sns_client.subscribe(TopicArn=topic_arn, Protocol = 'sqs', Endpoint = queue_arn)
response = sqs_client.set_queue_attributes(
    QueueUrl = write_queue.url,
    Attributes = {'Policy' : """{{"Version" : "2012-10-17",
                                 "Statement" : [
                               {{"Sid" : "MyPolicy",
                                 "Effect" : "Allow",
                                 "Principal" : {{"AWS" : "*"}},
                                 "Action" : "SQS:SendMessage",
                                 "Resource" : "{}",
                                 "Condition" : {{
                                 "ArnEquals" : {{
                                 "aws:SourceArn" : "{}"}}}}}}]}}""".format(queue_arn, topic_arn)})

#Set up Response Queue
response_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})

#Deletes queue when program is terminated
def ExitHandler():
    session.client('sqs').delete_queue(QueueUrl = response_queue.url)
    session.client('sqs').delete_queue(QueueUrl = write_queue.url)
atexit.register(ExitHandler)

def CreateCookie(args, author):
    user, = args
    cookie = uuid.uuid4()
    print("Creating for", user, cookie, "from", author)
    time = binascii.hexlify(pickle.dumps(datetime.datetime.now())).decode("utf-8")
    sns_client.publish(TopicArn = topic_arn, Message = json.dumps({"Operation" : "Insert",
                                            "Args" : (str(cookie), user, time) }))
    sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"Cookie" : str(cookie)}))

def ValidateSession(args, author):
    cookie = uuid.UUID(args[0])
    print("Validating", cookie)
    exists = bool(cursor.execute("select Cookie from Cookies where Cookie = ?", [cookie]).fetchall()[0])
    sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"valid" : exists}))

def LogOut(args, author):
    print("Logging Out", args[0])
    cookie = binascii.hexlify(pickle.dumps(uuid.UUID(args[0]))).decode("utf-8")
    sns_client.publish(TopicArn = topic_arn, Message = json.dumps({"Operation" : "Delete",
                                                   "Args" : (cookie, WC, WC) }))

#Select from table
def Select(fields, sort, return_limit, search_data, author):
    cookie = pickle.loads(binascii.unhexlify(search_data[0]))
    user = pickle.loads(binascii.unhexlify(search_data[1]))
    time = pickle.loads(binascii.unhexlify(search_data[2]))
    print("Selecting", fields, "on", [cookie, user, time], "by", sort)
    query = "select {} from Cookies where Cookie like ? and AccountID like ? and TimeStamp like ? order by {}".format(fields, sort)
    table = cursor.execute(query, [cookie, user, time]).fetchall()
    if return_limit:
        table = table[:return_limit]
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))
    else:
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))

#Insert into table
def Insert(args):
    cookie = uuid.UUID(args[0])
    user = uuid.UUID(args[1])
    time = pickle.loads(binascii.unhexlify(args[2]))
    print("Insert", cookie, user, time)
    cursor.execute("insert into Cookies values (?, ?, ?)", [cookie, user, time])
    connection.commit()

#Delete from table
def Delete(args):
    cookie = pickle.loads(binascii.unhexlify(args[0]))
    user = pickle.loads(binascii.unhexlify(args[1]))
    time = pickle.loads(binascii.unhexlify(args[2]))
    print("Deleting", cookie, user, time)
    cursor.execute("delete from Cookies where Cookie like ? and AccountID like ? and TimeStamp like ?", [cookie, user, time])
    connection.commit()

def SendAndGetResponse(queue, message):
    queue.send_message(MessageBody = json.dumps(message),
        MessageAttributes = {'authorID': {'StringValue': microservice_name, 'DataType':'String'}})
    while True:
          for message in response_queue.receive_messages():
              messagebody = message.body
              message.delete()
              return(json.loads(messagebody))

functions = [name for name in dir() if name[0].isupper() and not name[0] == "_"]
functions = [name for name in functions if name not in ["ExitHandler", "SendAndGetResponse"]]

while True:
    for message in write_queue.receive_messages():
        try:
            request = json.loads(json.loads(message.body)["Message"])
            if request["Operation"] == "Update":
                #Do a Function or something I suppose
                pass
            else:
                function = request["Operation"]
                args = request["Args"]
                getattr(__import__(__name__), function)(args)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()

    for message in read_queue.receive_messages(MessageAttributeNames=['authorID']):
        try:
            request = json.loads(message.body)
            if "function" in request:
                call = request["function"].title()
                args = request["parameters"]
                author = message.message_attributes.get("authorID").get("StringValue")
                for function in functions:
                    if call.title() == function.title():
                        getattr(__import__(__name__), function)(args, author)
            elif "Operation" in request:
                author = message.message_attributes.get("authorID").get("StringValue")
                fields = request["ReturnFields"]
                search_data = request["SearchData"]
                sort = request["Sort"]
                return_limit = request["ResultLimit"]
                getattr(__import__(__name__), "Select")(fields, sort, return_limit, search_data, author)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()
