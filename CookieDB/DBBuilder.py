import sqlite3 as sqlite
import uuid
import datetime
import pickle

sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

def Build():
    cursor.execute("drop table if exists Cookies")
    cursor.execute("""create table Cookies (
                      Cookie GUID primary key,
                      AccountID GUID forein key,
                      TimeStamp datetime)""")

def TestDataTypes():
    table = cursor.execute("select * from Cookies").fetchall()
    [print(cookie) for cookie in table]
    print(pickle.dumps(table))

def Print():
    [print(cookie) for cookie in cursor.execute("""select * from Cookies""")]

#Build()
TestDataTypes()
#Print()
