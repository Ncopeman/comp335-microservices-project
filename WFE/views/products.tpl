%rebase('main.tpl')
<div class='content'  id='productContent'>
    %for product in products:
        <div class = 'productEntry panel'>
          <div>
            <div class='roundCrop productImage'>
              <img src="\static\images\box.png" height='100px'>
            </div>
          </div>
          <div style="padding-left:20px">
              <h4 class='verticalCenter productName productItem'>{{product['name']}}</h4>
              <p class='productDescription'>This is a description of the product. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis blandit id elit non interdum. Cras quis eros elit. Mauris tristique non felis egestas efficitur. Phasellus vitae odio sed erat efficitur feugiat eget quis ex. Suspendisse congue pharetra ex vitae consequat. Integer non iaculis lacus. Phasellus at pulvinar lectus. Sed ac fringilla elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce malesuada erat id pretium rhoncus. Aliquam euismod a risus vel vulputate. Curabitur blandit cursus elit a lacinia. </p>
          </div>
          <div class='verticalCenter productPrice' style='float:right; margin-left:auto'>
            <h4 class='verticalCenter'>${{product['price']}}</h4>
            <a href='/addtocart/{{product['productID']}}'style='margin:10 0 0 0; font-size:14'>Add to cart</a>
          </div>
        </div>
      %end
</div>
