%rebase('main.tpl')
<html>
<head lang = "en">
  <meta charset="UTF-8">
  <link rel="stylesheet" href="/static/style.css" type="text/css">
  <title>We need a title...</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300i,300,400,500,700" rel="stylesheet">
</head>
<body style="font-family: 'roboto', sans-serif">
  <div class="panel" id='loginPanel'>
    <div>
      <img id='loginlogo' src="">
    </div>
    <div>
      <form id='loginForm' action='/login' method="post">
        Username:<br><input type="text" class='loginFormInput' name='user'></input><br>
        Password:<br><input type="password" class='loginFormInput' name='password'></input><br>
        <input type="submit" value="Log In" id='loginFormButton'>
      </form>
    </div>
    % if badLogin:
      <p id = 'badLogin'>Invalid Username/Password.<br>Please Try Again</p>
    % end
  </div>
</body>
