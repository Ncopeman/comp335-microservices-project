<!--Main template for the dashboard interace. Contains the navigation bar-->
<html>

<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/static/style.css" type="text/css">
    <title>We need a title...</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
</head>

<body style="font-family: 'roboto', sans-serif">
    <div id="headerBar">
        <div style="max-width: 60em; min-width:500px; margin: 0 auto; padding-left: 20px; padding-right:20px;">
            <div id="siteName">
                <h3 style="margin:0" ><a style="color:black" href="\">COMP335 Retail PaaS: Tennant 1</a></h3>
                % if userName is not None:
                    <p id="loginStatus">Logged in as {{userName}}. (<a href="/logout">logout</a>)</p>
                % else:
                    <p id="loginStatus"><a href="/login">Login</a></p>
                %end
            </div>
            <div id="nav">
                <div id="search">
                    <form id='searchForm' action='/products' method="post">
                        <input type="text" class='searchQuery' name='searchQuery' placeholder="Search"></input>
                        <input type="image" src="/static/images/search.png" id='searchButton'>
                    </form>
                </div>
                <table>
                    <tr>
                        <td><a href="\products">Products</a></td>
                        <td><a href="\cart">My Cart</a></td>
                        <td><a href="\orders">My Orders</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!--This is where you put the stuff.-->
    {{!base}}
    </div>
</body>
