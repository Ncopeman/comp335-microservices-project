%rebase('main.tpl')
<div class='content'  id='productContent'>
    %for product in products:
        <div class = 'productEntry panel'>
          <div style="padding-left:20px">
              <h4 class='verticalCenter productName productItem' style="margin-top:5px">{{product['name']}}</h4>
          </div>
          <div class='verticalCenter productPrice' style='float:right; margin-left:auto;'>
             <table>
                 <tr>
                     <td><form method="post" style="margin:0">
                            <input style="text-align: center; width:3em" type="number" min="1" max="99" size="1" value={{product['quantity']}} name='quantity'>
                            <input type="hidden" name="productID" value={{product['productID']}}>
                            <input type="submit" value="Update"/></form></td>
                     <td><h4 class='verticalCenter'>${{product['price']}}</h4></td>
                     <td><a href="/removeFromCart/{{product['productID']}}"style='margin:10 0 0 0; font-size:14'>Remove</a></td>
                 </tr>
             </table>
          </div>
        </div>
    %end
    %if len(products) != 0:
        <div class = 'productEntry panel'>
          <div class='verticalCenter productPrice' style='float:right; margin-left:auto'>
             <table>
                 <tr>
                     <td><h4 class='verticalCenter'>Total Cost: ${{totalCost}}</h4></td>
                     <td><a href="/placeorder"style='margin-left:10; font-size:14; width: 18.5em;'>Place Order</a></td>
                 </tr>
             </table>
          </div>
        </div>
    %else:
        <div class = 'productEntry panel' style="justify-content:center">
          <h3 style="margin:6 0 0">Your cart is empty!<h3>
        </div>
    %end
</div>
