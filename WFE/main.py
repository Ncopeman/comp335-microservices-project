from bottle import Bottle, route, run, template, static_file, redirect, request, response
from cheroot.wsgi import Server as WSGIServer
import boto3, hashlib, binascii, uuid, json, atexit

comp335 = Bottle()
tennantName = "Tennant1"

#authenticate with IAM
session = boto3.Session(
aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ',
aws_secret_access_key='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',
region_name='ap-southeast-2'
)

# Generate a hostID, used to identify the service in SQS receive_messages
hostID = str(uuid.uuid4())

# Connect to queues
sqs = session.resource('sqs')
productQueue = sqs.get_queue_by_name(QueueName=tennantName+"_products")
accountQueue = sqs.get_queue_by_name(QueueName=tennantName+"_accounts")
cartQueue = sqs.get_queue_by_name(QueueName=tennantName+"_carts")
orderQueue = sqs.get_queue_by_name(QueueName=tennantName+"_orders")
cookieQueue = sqs.get_queue_by_name(QueueName=tennantName+"_cookies")
# All responses will be directed along this queue, identifiable by this hosts ID
responseQueue = sqs.create_queue(QueueName=hostID, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})
print('Response queue created with ARN of ' + responseQueue.attributes['QueueArn'])
# Deletes queue when program is terminated
def exitHandler():
    session.client('sqs').delete_queue(QueueUrl = responseQueue.url)
    print('Queue Deleted')
atexit.register(exitHandler)


# Root page, redirect to product page
@comp335.route('/')
def home():
    redirect('/products')


# Displays a listing of all products
@comp335.route('/products')
def products():
    # Request Products                               Search  Sort/Order   Max.  Page No.
    message = {"function":"getProducts", "parameters":("%", "Name Desc", 100, 1)}
    productsList = sendReceive(message, productQueue)
    return template('products', userName = getUserName(), products = productsList['products'])

# Display all products in cart
@comp335.route('/cart')
def cart():
    #If not logged in, redirect to login page
    if validateSession() is False:
        redirect('/login')
        # Get list of products in cart, and total cost.
    message = {"function":"getCart", "parameters":[request.get_cookie('sessionID')]}
    responseMessage = sendReceive(message, cartQueue)
    return template('cart', userName= getUserName(), totalCost = responseMessage['totalCost'], products = responseMessage['products'])


# Called when updating the quantities of items in the cart
@comp335.route('/cart', method="POST")
def updateCartQuantity():
    productID = request.forms.get('productID')
    quantity = request.forms.get('quantity')
    if validateSession() is False:
        redirect('/login')
    #update cart message, with quantity,
    message = {"function":"updateCart", "parameters":(request.get_cookie('sessionID'), productID, quantity)}
    #wait for a response from the queue, so that the user is not redirected before the change is made
    sendRecieve(message, cartQueue)
    redirect('/cart')



# Displays a list of the user's previous orders, price, and DataType
@comp335.route('/orders')
def orders():
    return template('orders',userName= getUserName())


#Adds the productID to the current users's cart
@comp335.route('/addtocart/<productID>')
def addToCart(productID):
    if validateSession() is False:
        redirect('/login')
    message = {"function":"addToCart", "parameters":(request.get_cookie('sessionID'), productID)}
    print(message)
    sendOnly(message, cartQueue)
    redirect('/cart')


#Removes the given productID from the users' cart
@comp335.route('/removeFromCart/<productID>')
def removeFromCart(productID):
    if validateSession() is False:
        redirect('/login')
    #update cart message, with a quantity of zero,
    message = {"function":"updateCart", "parameters":(request.get_cookie('sessionID'), productID, 0)}
    #wait for a response from the queue, so that the user is not redirected before the change is made
    sendRecieve(message, cartQueue)
    redirect('cart')

# Login Page
@comp335.route('/login')
def login():
    return template('login', userName = None, badLogin=False)


# Login details submitted. For now, just show login failed.
@comp335.route('/login', method="POST")
def loginAttempt():
    user = request.forms.get('user')
    password = request.forms.get('password')
    # Pass login details to accounts microservice
    message = {"function":"login", "parameters":(user,encode(password))}
    print(message)
    responseMessage = sendReceive(message, accountQueue)
    #If login successful
    if responseMessage['LoggedIn'] is True:
        print('login success')
        #user login successful. Apply cookie.
        print(responseMessage['Cookie'])
        response.set_cookie('sessionID', responseMessage['Cookie'], path="/")
        redirect('/')
    return template('login', userName = None, badLogin=True)


# Tell the accounts microservice to forget the sessionID of the user
@comp335.route('/logout')
def logout():
    #Logout mesasge                                                SessionID of user
    message = {'function':'logout', 'parameters':[request.get_cookie('sessionID')]}
    # Send via cookiequeue, don't wait for a response
    sendOnly(message, cookieQueue)
    # Remove the cookie from the user, redirect to home.
    response.delete_cookie('sessionID')
    redirect('/')


#Determines if the sessionID present in the user's cookie is still valid.
def validateSession():
    message = {'function':'validateSession', 'parameters':[request.get_cookie('sessionID')]}
    responseMessage = sendReceive(message, cookieQueue)
    return responseMessage['valid']


# Serve static files (but only inside the static folder)
@comp335.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


# Sends the given message to the given queue, and
# Waits for a message to appear in the queue with this programs hostID. returns the message.
def sendReceive(message, queue):
    queue.send_message(MessageBody = json.dumps(message), MessageAttributes = {'authorID': {'StringValue': hostID, 'DataType':'String'}})
    while True:
        for message in responseQueue.receive_messages():
            messageBody = json.loads(message.body)
            message.delete()
            print("RESPONSE: " + str(messageBody))
            return messageBody


#sends the message on the given queue, but doesn't wait for a response
def sendOnly(message, queue):
    queue.send_message(MessageBody = json.dumps(message), MessageAttributes = {'authorID': {'StringValue': hostID, 'DataType':'String'}})


# Returns the current user's full name if they are logged in, otherwise None.
def getUserName():
    if request.get_cookie('sessionID') is None:
        return None
    print("SessionID exists, checking validity")
    if validateSession is False:
        redirect('/logout')
    message = {'function':'getFullName', 'parameters':[request.get_cookie('sessionID')]}
    responseMessage = sendReceive(message, accountQueue);
    return responseMessage['name']


# Returns a hashed version of the given string, in hexadecimal.
def encode(password):
        salt = b'randomStringIsRandom'
        dk = hashlib.pbkdf2_hmac('sha256', bytes(password, 'utf-8'), salt, 100000)
        return binascii.hexlify(dk).decode('utf-8')


# Start the server
if __name__ == "__main__":
    #Change these values to set the IP address and port for the server
    IP_ADDR = '127.0.0.1'
    PORT = 8080
    server = WSGIServer(
        (IP_ADDR, PORT),
        comp335,
        server_name='comp335',
        numthreads=30)
    print("Server started on {}, port {}. Closing this window will close the server".format(IP_ADDR, PORT))
    server.start()
