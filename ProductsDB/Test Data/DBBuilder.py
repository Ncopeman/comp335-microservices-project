import sqlite3 as sqlite
import uuid

sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

def Build():
    cursor.execute("drop table if exists Products")
    cursor.execute("""create table Products (
                      ID GUID primary key,
                      Name text,
                      Description text,
                      Price float)""")
    connection.commit()

def TestData():
    file = open("Products.tsv", "r").readlines()[1:]
    for line in file:
        Name, Description, Price = line.split("\t")
        print(Name, Description, Price)
        Price = float(Price)
        Name = Name.replace("&amp;", "&")
        args = [uuid.uuid4(), Name, Description, Price]
        cursor.execute("insert into Products values (?, ?, ?, ?)", args)
    connection.commit()

def Print():
    [print(product) for product in cursor.execute("select * from Products").fetchall()]


Build()
TestData()
Print()
