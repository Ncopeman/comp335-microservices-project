import json

data = """
('ec:addImpression', { 'id': '240332', 'name': 'Disney - Gargoyles - Goliath Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 1, 'price': 19.00 });
('ec:addImpression', { 'id': '238312', 'name': 'Halo - Master Chief with Cortana Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 2, 'price': 19.00 });
('ec:addImpression', { 'id': '238313', 'name': 'Halo - ODST Buck Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 3, 'price': 19.00 });
('ec:addImpression', { 'id': '238517', 'name': 'Marvel - Deadpool - Pandapool Playtime Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 4, 'price': 19.00 });
('ec:addImpression', { 'id': '235535', 'name': 'The Joy Of Painting - Bob Ross with Peapod Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 5, 'price': 19.00 });
('ec:addImpression', { 'id': '236915', 'name': 'Disney - Mickey Mouse Organization XIII Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 6, 'price': 19.00 });
('ec:addImpression', { 'id': '238496', 'name': 'Garbage Pail Kids - Adam Bomb Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 7, 'price': 19.00 });
('ec:addImpression', { 'id': '239707', 'name': 'New Girl - Nick Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 8, 'price': 19.00 });
('ec:addImpression', { 'id': '239708', 'name': 'New Girl - Winston Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 9, 'price': 19.00 });
('ec:addImpression', { 'id': '239710', 'name': 'New Girl - Jess Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 10, 'price': 19.00 });
('ec:addImpression', { 'id': '239711', 'name': 'Baby Driver - Bats Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 11, 'price': 19.00 });
('ec:addImpression', { 'id': '239714', 'name': 'Coming to America - Prince Akeem Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 12, 'price': 19.00 });
('ec:addImpression', { 'id': '239715', 'name': 'Coming to America - Semmi Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 13, 'price': 19.00 });
('ec:addImpression', { 'id': '238905', 'name': 'Star Wars - Episode VIII - Sad Porg Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 14, 'price': 19.00 });
('ec:addImpression', { 'id': '238904', 'name': 'Star Wars - Episode VIII - C’ai Threnalli Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 15, 'price': 19.00 });
('ec:addImpression', { 'id': '238903', 'name': 'Star Wars - Episode VIII - Ahch-To Caretaker Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 16, 'price': 19.00 });
('ec:addImpression', { 'id': '238902', 'name': 'Star Wars - Episode VIII - Paige Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 17, 'price': 19.00 });
('ec:addImpression', { 'id': '238900', 'name': 'Star Wars - Episode VIII - Vulptex Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 18, 'price': 19.00 });
('ec:addImpression', { 'id': '238901', 'name': 'Star Wars - Episode VIII - Luke Skywalker Final Battle Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 19, 'price': 19.00 });
('ec:addImpression', { 'id': '238906', 'name': 'Star Wars - Episode VIII - Rey &amp; Praetorian Guard Battle Movie Moments 2-Pack Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 20, 'price': 50.00 });
('ec:addImpression', { 'id': '238907', 'name': 'Star Wars - Episode VIII - Kylo Ren &amp; Praetorian Guard Battle Movie Moments 2-Pack Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 21, 'price': 50.00 });
('ec:addImpression', { 'id': '238931', 'name': 'Guillermo del Toro Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 22, 'price': 19.00 });
('ec:addImpression', { 'id': '240822', 'name': 'Steven Universe - Pink Diamond Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 23, 'price': 19.00 });
('ec:addImpression', { 'id': '240309', 'name': 'Marvel - Avengers: Infinity War - Iron Man Red Chrome Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 24, 'price': 25.00 });
('ec:addImpression', { 'id': '240804', 'name': 'Spyro the Dragon - Spyro with Sparx Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 25, 'price': 19.00 });
('ec:addImpression', { 'id': '235056', 'name': 'NBA - Cleveland Cavaliers Kyrie Irving Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 26, 'price': 19.00 });
('ec:addImpression', { 'id': '238182', 'name': 'Marvel - Deadpool in Chicken Suit Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 27, 'price': 19.00 });
('ec:addImpression', { 'id': '238183', 'name': 'Marvel - Deadpool - Samurai Deadpool Playtime Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 28, 'price': 19.00 });
('ec:addImpression', { 'id': '240866', 'name': 'I Love Lucy - Ricky Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 29, 'price': 19.00 });
('ec:addImpression', { 'id': '240893', 'name': 'I Love Lucy - Lucy in Factory Uniform Pop! Vinyl Figure', 'brand': 'Toys &amp; Gadgets', 'category': 'Pop! Vinyls', 'list': 'Search Results', 'position': 30, 'price': 19.00 });
"""

lines = [json.loads(line[21:-2].replace("'", '"')) for line in data.split("\n")[1:-1]]

file = open("Products.tsv", "w")
file.write("Name\tDescription\tPrice\n")
for product in lines:
    name = product["name"]
    desc = "Pop! Vinyl"
    price = str(product["price"])
    file.write("\t".join([name, desc, price]) + "\n")
file.close()




