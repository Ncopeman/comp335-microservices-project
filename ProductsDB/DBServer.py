import boto3
import sqlite3 as sqlite
import json
import uuid
import binascii
import pickle
import atexit

tennant_name = "Tennant1"
microservice_name = str(uuid.uuid4()) #Required for Response or Write Queues
WC = binascii.hexlify(pickle.dumps("%")).decode("utf-8")

#Connect to DB and Define GUID Conversions
sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

#Build Sessions and Clients
session = boto3.Session(aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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', region_name='ap-southeast-2')
sqs = session.resource("sqs", region_name = "ap-southeast-2")
read_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_products")

#Set Up Write Queue
sqs_client = boto3.client("sqs", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
sns_client = boto3.client("sns", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
write_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})
queue_arn = write_queue.attributes['QueueArn']
change_queue = sns_client.list_topics()["Topics"]
for value in change_queue:
    value = value["TopicArn"]
    if tennant_name + "_products" in value:
        topic_arn = value
sns_client.subscribe(TopicArn=topic_arn, Protocol = 'sqs', Endpoint = queue_arn)
response = sqs_client.set_queue_attributes(
    QueueUrl = write_queue.url,
    Attributes = {'Policy' : """{{"Version" : "2012-10-17",
                                 "Statement" : [
                               {{"Sid" : "MyPolicy",
                                 "Effect" : "Allow",
                                 "Principal" : {{"AWS" : "*"}},
                                 "Action" : "SQS:SendMessage",
                                 "Resource" : "{}",
                                 "Condition" : {{
                                 "ArnEquals" : {{
                                 "aws:SourceArn" : "{}"}}}}}}]}}""".format(queue_arn, topic_arn)})
#Set up Response Queue
response_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})

#Deletes queue when program is terminated
def ExitHandler():
    session.client('sqs').delete_queue(QueueUrl = response_queue.url)
    session.client('sqs').delete_queue(QueueUrl = write_queue.url)
atexit.register(ExitHandler)

def GetProducts(args, author):
    search, sort, page_items, page = args
    page_start = (page - 1) * page_items
    page_end = page * page_items
    print("Selecting Products on", search, "by", sort)
    products = cursor.execute("""select ID, Name, Description, Price from Products
                              where Name like ? order by ?""", [search, sort]).fetchall()
    page_list = [{"productID" : str(ID), "name" : Name,
                  "description" : Description, "price" : format(Price, "0.2f")}
                  for ID, Name, Description, Price in products[page_start:page_end]]
    sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"products" : page_list}))

#Select from table
def Select(fields, sort, return_limit, search_data, author):
    ID = pickle.loads(binascii.unhexlify(search_data[0]))
    name = pickle.loads(binascii.unhexlify(search_data[1]))
    description = pickle.loads(binascii.unhexlify(search_data[2]))
    price = pickle.loads(binascii.unhexlify(search_data[3]))
    print("Selecting", fields, "on", [ID, name, description, price], "by", sort)
    query = """select {} from Products where ID like ? and Name like ? and Description like ?
            and Price like ? order by {}""".format(fields, sort)
    table = cursor.execute(query, [ID, name, description, price]).fetchall()
    if return_limit:
        table = table[:return_limit]
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))
    else:
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))

#Insert into table
def Insert(args):
    ID = uuid.UUID(args[0])
    name = args[1]
    description = args[2]
    price = float(args[3])
    print("Insert", ID, name, description, price)
    cursor.execute("insert into Cookies values (?, ?, ?, ?, ?)", [ID, name, description, price])
    connection.commit()

#Delete from table
def Delete(args):
    ID = pickle.loads(binascii.unhexlify(args[0]))
    name = pickle.loads(binascii.unhexlify(args[1]))
    description = pickle.loads(binascii.unhexlify(args[2]))
    price = pickle.loads(binascii.unhexlify(args[3]))
    print("Deleting", ID, name, description, price)
    cursor.execute("""delete from Cookies where ID like ? and Name like ? and Description like ?
            and Price like ?""", [ID, name, description, price])
    connection.commit()

def SendAndGetResponse(queue, message):
    queue.send_message(MessageBody = json.dumps(message),
        MessageAttributes = {'authorID': {'StringValue': microservice_name, 'DataType':'String'}})
    while True:
          for message in response_queue.receive_messages():
              messagebody = message.body
              message.delete()
              return(json.loads(messagebody))

functions = [name for name in dir() if name[0].isupper() and not name[0] == "_"]
functions = [name for name in functions if name not in ["ExitHandler", "SendAndGetResponse"]]

while True:
    for message in write_queue.receive_messages():
        try:
            request = json.loads(json.loads(message.body)["Message"])
            if request["Operation"] == "Update":
                #Do a Function or something I suppose
                pass
            else:
                function = request["Operation"]
                args = request["Args"]
                getattr(__import__(__name__), function)(args)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()

    for message in read_queue.receive_messages(MessageAttributeNames=['authorID']):
        try:
            request = json.loads(message.body)
            if "function" in request:
                call = request["function"].title()
                args = request["parameters"]
                author = message.message_attributes.get("authorID").get("StringValue")
                for function in functions:
                    if call.title() == function.title():
                        getattr(__import__(__name__), function)(args, author)
            elif "Operation" in request:
                author = message.message_attributes.get("authorID").get("StringValue")
                fields = request["ReturnFields"]
                search_data = request["SearchData"]
                sort = request["Sort"]
                return_limit = request["ResultLimit"]
                getattr(__import__(__name__), "Select")(fields, sort, return_limit, search_data, author)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()
