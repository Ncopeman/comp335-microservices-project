import boto3
import sqlite3 as sqlite
import json
import uuid
import atexit
import binascii
import pickle

tennant_name = "Tennant1"
microservice_name = str(uuid.uuid4()) #Required for Response or Write Queues
WC = binascii.hexlify(pickle.dumps("%")).decode("utf-8")

#Connect to DB and Define GUID Conversions
sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

#Build Sessions and Clients
session = boto3.Session(aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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', region_name='ap-southeast-2')
sqs = session.resource("sqs", region_name = "ap-southeast-2")
read_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_accounts")
cookie_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_cookies")

#Set Up Write Queue
sqs_client = boto3.client("sqs", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
sns_client = boto3.client("sns", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
write_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})
queue_arn = write_queue.attributes['QueueArn']
change_queue = sns_client.list_topics()["Topics"]
for value in change_queue:
    value = value["TopicArn"]
    if tennant_name + "_accounts" in value:
        topic_arn = value
sns_client.subscribe(TopicArn=topic_arn, Protocol = 'sqs', Endpoint = queue_arn)
response = sqs_client.set_queue_attributes(
    QueueUrl = write_queue.url,
    Attributes = {'Policy' : """{{"Version" : "2012-10-17",
                                 "Statement" : [
                               {{"Sid" : "MyPolicy",
                                 "Effect" : "Allow",
                                 "Principal" : {{"AWS" : "*"}},
                                 "Action" : "SQS:SendMessage",
                                 "Resource" : "{}",
                                 "Condition" : {{
                                 "ArnEquals" : {{
                                 "aws:SourceArn" : "{}"}}}}}}]}}""".format(queue_arn, topic_arn)})

#Set up Response Queue
response_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})

#Deletes queue when program is terminated
def ExitHandler():
    session.client('sqs').delete_queue(QueueUrl = response_queue.url)
    session.client('sqs').delete_queue(QueueUrl = write_queue.url)
atexit.register(ExitHandler)

def Login(args, author):
    #print("Hit", args[0])
    #return(True)
    username, password = args
    print("Login for", username, "from", author)
    try:
        ID = cursor.execute("select ID from Accounts where Username = ? and Password = ?", [username, binascii.unhexlify(password)]).fetchall()[0][0]
        cookie = SendAndGetResponse(cookie_queue, {"function" : "CreateCookie", "parameters" : [str(ID)]})["Cookie"]
        sqs.get_queue_by_name(QueueName = author).send_message(
                        MessageBody = json.dumps({"LoggedIn" : True, "Cookie" : cookie}))
    except IndexError:
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"LoggedIn" : False}))

def GetFullName(args, author):
    #print("Hit")
    #return(True)
    print("Get Full Name for", args[0])
    cookie = binascii.hexlify(pickle.dumps(uuid.UUID(args[0]))).decode("utf-8")
    accountID = SendAndGetResponse(cookie_queue, {"Operation" : "Select", "ReturnFields" : "AccountID",
                    "SearchData" : [cookie, WC, WC], "Sort" : "AccountID",
                    "ResultLimit" : 0})["data"]
    accountID = pickle.loads(binascii.unhexlify(accountID))[0]
    names = cursor.execute("select FirstName, LastName from Accounts where ID = ?", accountID).fetchall()[0]
    sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"name" : " ".join(names)}))

#Select from table
def Select(fields, sort, return_limit, search_data, author):
    ID = pickle.loads(binascii.unhexlify(search_data[0]))
    first_name = pickle.loads(binascii.unhexlify(search_data[1]))
    last_name = pickle.loads(binascii.unhexlify(search_data[2]))
    username = pickle.loads(binascii.unhexlify(search_data[3]))
    password = pickle.loads(binascii.unhexlify(search_data[4]))
    print("Selecting", fields, "on", [ID, first_name, last_name, username, password], "by", sort)
    query = """select {} from Accounts where ID like ? and FirstName like ? and LastName like ?
            and Username like ? and Password like ? order by {}""".format(fields, sort)
    table = cursor.execute(query, [ID, first_name, last_name, username, password]).fetchall()
    if return_limit:
        table = table[:return_limit]
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))
    else:
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))

#Insert into table
def Insert(args):
    ID = uuid.UUID(args[0])
    first_name = args[1]
    last_name = args[2]
    username = args[3]
    password = pickle.loads(binascii.unhexlify(args[4]))
    print("Insert", ID, first_name, last_name, username, password)
    cursor.execute("insert into Cookies values (?, ?, ?, ?, ?)", [ID, first_name, last_name, username, password])
    connection.commit()

#Delete from table
def Delete(args):
    ID = pickle.loads(binascii.unhexlify(args[0]))
    first_name = pickle.loads(binascii.unhexlify(args[1]))
    last_name = pickle.loads(binascii.unhexlify(args[2]))
    username = pickle.loads(binascii.unhexlify(args[3]))
    password = pickle.loads(binascii.unhexlify(args[4]))
    print("Deleting", ID, first_name, last_name, username, password)
    cursor.execute("""delete from Cookies where ID like ? and FirstName like ? and LastName like ? and\
                    Username like ? and Password like ?""", [ID, first_name, last_name, username, password])
    connection.commit()

def SendAndGetResponse(queue, message):
    queue.send_message(MessageBody = json.dumps(message),
        MessageAttributes = {'authorID': {'StringValue': microservice_name, 'DataType':'String'}})
    while True:
          for message in response_queue.receive_messages():
              messagebody = message.body
              message.delete()
              return(json.loads(messagebody))

functions = [name for name in dir() if name[0].isupper() and not name[0] == "_"]
functions = [name for name in functions if name not in ["ExitHandler", "SendAndGetResponse"]]

while True:
    for message in write_queue.receive_messages():
        try:
            request = json.loads(json.loads(message.body)["Message"])
            if request["Operation"] == "Update":
                #Do a Function or something I suppose
                pass
            else:
                function = request["Operation"]
                args = request["Args"]
                getattr(__import__(__name__), function)(args)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()

    for message in read_queue.receive_messages(MessageAttributeNames=['authorID']):
        try:
            request = json.loads(message.body)
            if "function" in request:
                call = request["function"].title()
                args = request["parameters"]
                author = message.message_attributes.get("authorID").get("StringValue")
                for function in functions:
                    if call.title() == function.title():
                        getattr(__import__(__name__), function)(args, author)
            elif "Operation" in request:
                author = message.message_attributes.get("authorID").get("StringValue")
                fields = request["ReturnFields"]
                search_data = request["SearchData"]
                sort = request["Sort"]
                return_limit = request["ResultLimit"]
                getattr(__import__(__name__), "Select")(fields, sort, return_limit, search_data, author)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()
    
