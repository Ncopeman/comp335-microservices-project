import sqlite3 as sqlite
import random
import uuid
import os
import hashlib

sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

def Build():
    cursor.execute("drop table if exists Accounts")
    cursor.execute("""create table Accounts (
                      ID GUID primary key,
                      FirstName text,
                      LastName text,
                      Username text,
                      Password blob)""")

def TestData():
    male_file = open(os.path.join(os.path.dirname(__file__), "male.txt"), 'r')
    female_file = open(os.path.join(os.path.dirname(__file__), "female.txt"), 'r')
    password_file = open(os.path.join(os.path.dirname(__file__), "password bases.txt"), 'r')
    names = [name.strip() for name in male_file.readlines()]
    names.extend([name.strip() for name in female_file.readlines()])
    password_bases = [ipsum.strip() for ipsum in password_file.readlines()]
    emails = ["@gmail.com", "@hotmail.com", "@live.com", "@outlook.com"]
    male_file.close()
    female_file.close()
    password_file.close()

    plaintext = open(os.path.join(os.path.dirname(__file__), "Test Data.tsv"), 'w')
    plaintext.write("GUID\tFirstName\tLastName\tUsername\tPassword\n")
    for count in range(20):
        ID = uuid.uuid4()
        FirstName = random.choice(names).title()
        LastName = random.choice(names).title()
        Username = random.choice(names) + random.choice(emails)
        Password = random.choice(password_bases) + str(random.randint(0, 9))
        if random.randint(0, 1):
            Password = Password.title()
        salt = b'randomStringIsRandom'
        hashp = hashlib.pbkdf2_hmac('sha256', bytes(Password, 'utf-8'), salt, 100000)
        cursor.execute("insert into Accounts values (?, ?, ?, ?, ?)", [ID, FirstName, LastName, Username, hashp])
        plaintext.write("\t".join([str(ID), FirstName, LastName, Username, Password]) + "\n")
    plaintext.close()
    connection.commit()

def Read():
    [print(row) for row in cursor.execute("select * from Accounts").fetchall()]
        
    

Build()
TestData()
Read()
