import boto3
import sqlite3 as sqlite
import json
import uuid
import binascii
import pickle
import atexit

tennant_name = "Tennant1"
microservice_name = str(uuid.uuid4()) #Required for Response or Write Queues
WC = binascii.hexlify(pickle.dumps("%")).decode("utf-8")

#Connect to DB and Define GUID Conversions
sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

#Build Sessions and Clients
session = boto3.Session(aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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', region_name='ap-southeast-2')
sqs = session.resource("sqs", region_name = "ap-southeast-2")
read_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_carts")
cookie_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_cookies")
product_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_products")

#Set Up Write Queue
sqs_client = boto3.client("sqs", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
sns_client = boto3.client("sns", region_name = "ap-southeast-2", aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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')
write_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})
queue_arn = write_queue.attributes['QueueArn']
change_queue = sns_client.list_topics()["Topics"]
for value in change_queue:
    value = value["TopicArn"]
    if tennant_name + "_carts" in value:
        topic_arn = value
sns_client.subscribe(TopicArn=topic_arn, Protocol = 'sqs', Endpoint = queue_arn)
response = sqs_client.set_queue_attributes(
    QueueUrl = write_queue.url,
    Attributes = {'Policy' : """{{"Version" : "2012-10-17",
                                 "Statement" : [
                               {{"Sid" : "MyPolicy",
                                 "Effect" : "Allow",
                                 "Principal" : {{"AWS" : "*"}},
                                 "Action" : "SQS:SendMessage",
                                 "Resource" : "{}",
                                 "Condition" : {{
                                 "ArnEquals" : {{
                                 "aws:SourceArn" : "{}"}}}}}}]}}""".format(queue_arn, topic_arn)})

#Set up Response Queue
response_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})

#Deletes queue when program is terminated
def ExitHandler():
    session.client('sqs').delete_queue(QueueUrl = response_queue.url)
    session.client('sqs').delete_queue(QueueUrl = write_queue.url)
atexit.register(ExitHandler)

def AddToCart(args, author):
    cookie = binascii.hexlify(pickle.dumps(uuid.UUID(args[0]))).decode("utf-8")
    product = uuid.UUID(args[1])
    print("Adding to cart", uuid.UUID(args[0]))
    accountID = SendAndGetResponse(cookie_queue, {"Operation" : "Select", "ReturnFields" : "AccountID",
                    "SearchData" : [cookie, WC, WC], "Sort" : "AccountID",
                    "ResultLimit" : 0})["data"]
    accountID = pickle.loads(binascii.unhexlify(accountID))[0][0]
    quantity = cursor.execute("select Quantity from Cart where ProductID = ? and OrderID = ?", [product, accountID]).fetchall()
    raw_product = binascii.hexlify(pickle.dumps(product)).decode("utf-8")
    print(quantity)
    if quantity:
        raw_accountID = binascii.hexlify(pickle.dumps(accountID)).encode("utf-8")
        sns_client.publish(TopicArn = topic_arn, Message = json.dumps({"Operation" : "Update",
                           "Search" : [raw_product, raw_accountID, WC], "Update Fields" : ["Quantity"],
                           "Update Data" : quantity[0][0] + 1}))
    else:
        sns_client.publish(TopicArn = topic_arn, Message = json.dumps({"Operation" : "Insert",
                           "parameters" : [str(product), str(accountID), 1]}))

def UpdateCart(args, author):
    cookie = binascii.hexlify(pickle.dumps(uuid.UUID(args[0])))
    product = args[1]
    quantity = args[2]
    print("Updating Cart", uuid.UUID(cookie, product))
    accountID = SendAndGetResponse(cookie_queue, {"Operation" : "Select", "ReturnFields" : "AccountID",
                    "SearchData" : [cookie, WC, WC], "Sort" : "AccountID",
                    "ResultLimit" : 0})["data"]
    accountID = pickle.loads(binascii.unhexlify(raw_accountID))[0]
    if quantity:
        sns_client.publish(TopicArn = topic_arn, Message = json.dumps({"Operation" : "Insert",
                               "parameters" : [product, str(accountID), quantity]}))
    else:
        sns_client.publish(TopicArn = topic_arn, Message = json.dumps({"Operation" : "Delete",
                               "parameters" : [product, str(accountID), quantity]}))

def GetCart(args, author):
    cookie = binascii.hexlify(pickle.dumps(uuid.UUID(args[0]))).decode("utf-8")
    print("Getting Cart", uuid.UUID(args[0]))
    accountID = SendAndGetResponse(cookie_queue, {"Operation" : "Select", "ReturnFields" : "AccountID",
                    "SearchData" : [cookie, WC, WC], "Sort" : "AccountID",
                    "ResultLimit" : 0})["data"]
    accountID = pickle.loads(binascii.unhexlify(accountID))[0]
    table = cursor.execute("select ProductID, Quantity from Cart where OrderID = ?", accountID).fetchall()
    total = 0
    cart = []
    for productID, quantity in table:
        send_productID = binascii.hexlify(pickle.dumps(productID)).decode("utf-8")
        product_data = SendAndGetResponse(product_queue, {"Operation" : "Select", "ReturnFields" : "Name, Price",
                    "SearchData" : [send_productID, WC, WC, WC], "Sort" : "Name",
                    "ResultLimit" : 0})["data"]
        product_data = pickle.loads(binascii.unhexlify(product_data))
        name, price = product_data[0]
        total += price * quantity
        cart.append({"productID" : str(productID), "quantity" : quantity, "name" : name, "price" : format(price * quantity, "0.2f")})
    sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"products" : cart, "totalCost" : format(total, "0.2f")}))

#Select from table
def Select(fields, sort, return_limit, search_data, author):
    productID = pickle.loads(binascii.unhexlify(search_data[0]))
    orderID = pickle.loads(binascii.unhexlify(search_data[1]))
    quantity = pickle.loads(binascii.unhexlify(search_data[2]))
    print("Selecting", fields, "on", [productID, orderID, quantity], "by", sort)
    query = """select {} from Cart where ProductID like ? and OrderID like ? and Quantity like ? order by {}""".format([fields, sort])
    table = cursor.execute(query, [productID, orderID, quantity])
    if return_limit:
        table = table[:return_limit]
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))
    else:
        table = binascii.hexlify(pickle.dumps(table)).decode("utf-8")
        sqs.get_queue_by_name(QueueName = author).send_message(MessageBody = json.dumps({"data" : table}))

#Insert into table
def Insert(args):
    productID = uuid.UUID(search_data[0])
    orderID = uuid.UUID(search_data[1])
    quantity = search_data[2]
    print("Insert", productID, orderID, quantity)
    cursor.execute("insert into Cart values (?, ?, ?)", [productID, orderID, quantity])
    connection.commit()

#Delete from table
def Delete(args):
    productID = pickle.loads(binascii.unhexlify(search_data[0]))
    orderID = pickle.loads(binascii.unhexlify(search_data[1]))
    quantity = pickle.loads(binascii.unhexlify(search_data[2]))
    print("Deleting", productID, orderID, quantity)
    cursor.execute("""delete from Cart where ProductID like ? and OrderID like ? and Quantity like ?""", [orderID, productID, quantity])
    connection.commit()

#Update
def Update(search, update_fields, update_data):
    search_fields = [pickle.loads(binascii.unhexlify(search_datum)) for search_datum in search_fields]
    update_data = [pickle.loads(binascii.unhexlify(update_datum)) for update_datum in update_data]
    update_fields = [field + " = ?" for field in update_fields]
    update_fields = update_fields.join(", ")
    print("Updating", update_data)
    query = "update Cart set {} where ProductID like ? and OrderID like ? and Quantity like ?".format(update_fields)
    prtin(query, update_fields)
    inserts = update_data.extend(search)
    cursor.execute(query, [orderID, productID, quantity])
    connection.commit()

def SendAndGetResponse(queue, message):
    queue.send_message(MessageBody = json.dumps(message),
        MessageAttributes = {'authorID': {'StringValue': microservice_name, 'DataType':'String'}})
    while True:
          for message in response_queue.receive_messages():
              messagebody = message.body
              message.delete()
              return(json.loads(messagebody))

functions = [name for name in dir() if name[0].isupper() and not name[0] == "_"]
functions = [name for name in functions if name not in ["ExitHandler", "SendAndGetResponse"]]

while True:
    for message in write_queue.receive_messages():
        try:
            request = json.loads(json.loads(message.body)["Message"])
            if request["Operation"] == "Update":
                search = request["Search"]
                update_fields = request["Update Fields"]
                update_data = request["Update Data"]
                Update(search, update_fields, update_data)
            else:
                function = request["Operation"]
                args = request["Args"]
                getattr(__import__(__name__), function)(args)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()

    for message in read_queue.receive_messages(MessageAttributeNames=['authorID']):
        try:
            request = json.loads(message.body)
            if "function" in request:
                call = request["function"].title()
                args = request["parameters"]
                author = message.message_attributes.get("authorID").get("StringValue")
                for function in functions:
                    if call.title() == function.title():
                        getattr(__import__(__name__), function)(args, author)
            elif "Operation" in request:
                author = message.message_attributes.get("authorID").get("StringValue")
                fields = request["ReturnFields"]
                search_data = request["SearchData"]
                sort = request["Sort"]
                return_limit = request["ResultLimit"]
                Select(fields, sort, return_limit, search_data, author)
            message.delete()
        except Exception as e:
            print(e)
            message.delete()
