import sqlite3 as sqlite
import uuid
import random
import pickle
import binascii
import boto3
import json

sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

tennant_name = "Tennant1"
microservice_name = str(uuid.uuid4()) #Required for Response or Write Queues
WC = binascii.hexlify(pickle.dumps("%")).decode("utf-8")
session = boto3.Session(aws_access_key_id='AKIAJQPCX4HCF2XNIVCQ', aws_secret_access_key='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', region_name='ap-southeast-2')
sqs = session.resource("sqs", region_name = "ap-southeast-2")
products_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_products")
accounts_queue = sqs.get_queue_by_name(QueueName = tennant_name + "_accounts")
response_queue = sqs.create_queue(QueueName = microservice_name)#, Attributes={'ReceiveMessageWaitTimeSeconds': '20'})


def Build():
    cursor.execute("drop table if exists Cart")
    cursor.execute("""create table Cart (
                      ProductID GUID foriegn key,
                      OrderID GUID foriegn key,
                      Quantity integer)""")
    connection.commit()

def TestData():
    accounts = SendAndGetResponse(accounts_queue, {"Operation" : "Select", "ReturnFields" : "ID",
                    "SearchData" : [WC, WC, WC, WC, WC], "Sort" : "ID",
                    "ResultLimit" : 0})["data"]
    accounts = pickle.loads(binascii.unhexlify(accounts))
    products = SendAndGetResponse(products_queue, {"Operation" : "Select", "ReturnFields" : "ID",
                    "SearchData" : [WC, WC, WC, WC], "Sort" : "ID",
                    "ResultLimit" : 0})["data"]
    products = pickle.loads(binascii.unhexlify(products))
    for account in accounts:
        account, = account
        for product in random.sample(products, random.randint(0, 7)):
            product, = product
            quantity = random.randint(1, 4)
            print(product, account, quantity)
            cursor.execute("insert into Cart values (?, ?, ?)", [product, account, quantity])
    connection.commit()

    
def SendAndGetResponse(queue, message):
    queue.send_message(MessageBody = json.dumps(message),
        MessageAttributes = {'authorID': {'StringValue': microservice_name, 'DataType':'String'}})
    while True:
          for message in response_queue.receive_messages():
              messagebody = message.body
              message.delete()
              return(json.loads(messagebody))

def Print():
    [print(items) for items in cursor.execute("select * from Cart")]

#Build()
#TestData()
Print()
