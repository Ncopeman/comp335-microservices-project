import sqlite3 as sqlite
import uuid

sqlite.register_converter('GUID', lambda b: uuid.UUID(bytes_le=b))
sqlite.register_adapter(uuid.UUID, lambda u: u.bytes_le)
connection = sqlite.connect("Database.db", detect_types = sqlite.PARSE_DECLTYPES)
cursor = connection.cursor()

def Build():
    cursor.execute("drop table if exists Orders")
    cursor.execute("""create table Orders (
                      ID GUID primary key,
                      CustomerID GUID foriegn key,
                      Date datetime,
                      Total float)""")
    connection.commit()

Build()
